##Summary:

Program allows user to work with sweets in gift.
The following operation will be executed:
#####1. Initialization of the gift:
      - from file;
      - from json;
      - from db;
      - program initialize gift with predefined values      

#####2. User can perform the following operation with gift:
      - sort by weight;
      - remove sweet from gift;
      - find sweet in gift less than weight and calories;

#####3. Write gift to:
      - txt file;
      - console;


## Installation and Getting Started:
     0) Program use **PostgreSQL DB**.
        DB parameters can be changed in DBGiftStorage.
     1) For working with DB user should have "GiftDB"
         You should run **"db_init.sql"** DB initialization script         
     2) User is able to read and write from/to JSON and TXT
      Example of initialization gift from JSON, look at **"jsonExampleFile"**
     3) The needed modules are located in **libs** 

## Modules
      Jackson
      PostgreSQL Driver