package com.task2.storage;

public class StorageFactory {
    /**
     * Method allows to selectwith wich storage user will be work:
     *
     * @param choice:        2 - Json
     *                       3 - DB
     *                       all other - TXT file
     * @param additionalInfo - path to JSON/.TXT file
     * @return giftStorage obejct
     */
    public static GiftStorage getStorage(final int choice, final String additionalInfo) {
        switch (choice) {
            case 3:
                return new DBGiftStorage();
            case 2:
                return new JsonGiftStorage(additionalInfo);
            case 1:
            default:
                return new SerializableFileGiftStorage(additionalInfo);
        }
    }
}
