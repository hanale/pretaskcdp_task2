package com.task2.storage;

import com.task2.model.Gift;

/**
 * GiftStorage interface for read & write operation in storage
 */
public interface GiftStorage {
    /**
     * Method allows write gift object into storage
     *
     * @param gift object to write
     * @throws Exception throws in case of any unexpected situations
     */
    void writeToStorage(final Gift gift) throws Exception;

    /**
     * Method allows read gift object from storage
     *
     * @return gift from storage
     * @throws Exception throws in case of any unexpected situations
     */
    Gift readFromStorage(final String filter) throws Exception;
}
