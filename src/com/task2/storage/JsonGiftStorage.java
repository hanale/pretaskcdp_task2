package com.task2.storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.task2.model.Gift;

import java.io.File;

/**
 * Class implement work with JSON
 * Class allows to read/write operations with file.
 * Gift object will be saved as JSON.
 * Write operation currently unsupported.
 *
 * @see GiftStorage for more info about implemented methods.
 * @see Gift
 */
public class JsonGiftStorage extends FileGiftStorage {

    /**
     * Construct a new class for I/O operations with simple .json file
     *
     * @param filePath - full path and the file's name under which will be executed operations
     */
    public JsonGiftStorage(final String filePath) {
        super(filePath);
    }

    public void writeToStorage(final Gift gift) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(filePath), gift);
    }

    public Gift readFromStorage(final String filter) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Gift gift = mapper.readValue(new File(filePath), Gift.class);
        return gift;
    }
}
