package com.task2.storage;

import com.task2.model.Gift;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Class allows to read/write operations with file.
 * I/O operations use object serialization mechanism.
 *
 * @see java.io.Serializable
 * @see GiftStorage for more info about implemented methods.
 * @see Gift
 */
public class SerializableFileGiftStorage extends FileGiftStorage {

    public SerializableFileGiftStorage(final String filePath) {
        super(filePath);
    }

    public void writeToStorage(final Gift gift) throws Exception {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
            oos.writeObject(gift);
        }
    }

    public Gift readFromStorage(final String filter) throws Exception {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filePath))) {
            return (Gift) in.readObject();
        }
    }
}
