package com.task2.storage;

import com.task2.model.Chocolate;
import com.task2.model.Gift;
import com.task2.model.Lollipop;
import com.task2.model.Sweet;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class allows to read/write operations with PostgreSQL storage.
 * Write operation currently unsupported.
 *
 * @see GiftStorage for more info about implemented methods.
 * @see Gift
 */
public class DBGiftStorage implements GiftStorage {
    private static final String URL = "jdbc:postgresql://localhost:5433/GiftDB";
    private static final String USER = "postgres";
    private static final String PASSWORD = "1";
    private static final String DB_DRIVER = "org.postgresql.Driver";
    private Map<String, Chocolate.ColorOfChocolate> chocolateType;
    private Map<String, Lollipop.FillingOfLollipop> fillingOfLollipopType;

    public DBGiftStorage() {
        initializeChocolateTypes();
        initializeFillingOfLollipopTypes();
    }


    public void writeToStorage(final Gift gift) throws Exception {
        throw new UnsupportedOperationException("Operation currently unsupported");
    }

    /**
     * Method read gift object from postgresql DB
     *
     * @param giftId - id of the gift
     * @return gift Gift from DB
     * @throws Exception any I/O exceptions connected with DB
     */
    public Gift readFromStorage(final String giftId) throws Exception {
        Gift gift = new Gift();
        try (Connection dbConnection = getDBConnection()) {
            Statement statement = dbConnection.createStatement();
            try (ResultSet rs = statement.executeQuery(getSqlRequest(giftId))) {
                while (rs.next()) {
                    Sweet sweet = createSweet(rs.getString("sweet_type"), rs);
                    if (!Objects.isNull(sweet)) {
                        gift.add(sweet);
                    }
                }
            }
            statement.close();
        }

        return gift;
    }

    private static Connection getDBConnection() {
        try {
            Class.forName(DB_DRIVER);
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your postgresql JDBC Driver?");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private String getSqlRequest(final String giftId) {
        return "SELECT * FROM gifts where gift_id='" + giftId + "'";
    }

    private Sweet createSweet(final String typeOfSweet, final ResultSet rs) {
        if ("Chocolate".equals(typeOfSweet)) {
            return getChocolate(rs);
        }
        if ("Lollipop".equals(typeOfSweet)) {
            return getLollipop(rs);
        }
        return null;
    }

    private Sweet getLollipop(final ResultSet rs) {
        try {
            String filling = rs.getString("sweet_filling");
            return new Lollipop(rs.getString("sweet_name"), rs.getString("sweet_producer"),
                    rs.getFloat("sweet_weight"), rs.getFloat("sweet_proteins"),
                    rs.getFloat("sweet_fats"), rs.getFloat("sweet_carbohydrates"),
                    rs.getFloat("sweet_calories"),
                    fillingOfLollipopType.getOrDefault(filling, Lollipop.FillingOfLollipop.None));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Sweet getChocolate(final ResultSet rs) {
        try {
            String color = rs.getString("sweet_color");
            return new Chocolate(rs.getString("sweet_name"), rs.getString("sweet_producer"),
                    rs.getFloat("sweet_weight"), rs.getFloat("sweet_proteins"),
                    rs.getFloat("sweet_fats"), rs.getFloat("sweet_carbohydrates"),
                    rs.getFloat("sweet_calories"),
                    chocolateType.getOrDefault(color, Chocolate.ColorOfChocolate.Unknown));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initializeChocolateTypes() {
        chocolateType = new HashMap<>();
        chocolateType.put("Black", Chocolate.ColorOfChocolate.Black);
        chocolateType.put("Milk", Chocolate.ColorOfChocolate.Milk);
        chocolateType.put("White", Chocolate.ColorOfChocolate.White);
    }

    private void initializeFillingOfLollipopTypes() {
        fillingOfLollipopType = new HashMap<>();
        fillingOfLollipopType.put("Alcohol", Lollipop.FillingOfLollipop.Alcohol);
        fillingOfLollipopType.put("Jam", Lollipop.FillingOfLollipop.Jam);
        fillingOfLollipopType.put("None", Lollipop.FillingOfLollipop.None);
        fillingOfLollipopType.put("Other", Lollipop.FillingOfLollipop.Other);
    }
}
