package com.task2.storage;

import com.task2.model.Gift;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Class allows to write into file in human readable style.
 * Write operation currently unsupported.
 *
 * @see GiftStorage for more info about implemented methods.
 * @see Gift
 */
public class SimpleTxtFileGiftStorage extends FileGiftStorage {

    /**
     * Construct a new class for I/O operations with simple .txt file
     *
     * @param filePath - full path and the file's name under which will be executed operations
     */
    public SimpleTxtFileGiftStorage(final String filePath) {
        super(filePath);
    }

    public void writeToStorage(final Gift gift) throws Exception {
        FileWriter writer = new FileWriter(filePath, false);
        try {
            writer.write(gift.toString());
            writer.flush();
            writer.close();
        } catch (IOException e){
            System.out.println("Some problem during writing to file");
            e.getMessage();
        }
    }


    public Gift readFromStorage(String filter) throws Exception {
        throw new UnsupportedOperationException("Operation currently unsupported");
    }
}
