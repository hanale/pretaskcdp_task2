package com.task2.storage;

/**
 * Abstract class for read/write operations with any kind of file e.g. txt, JSON, XML.
 * I/O operations use object serialization mechanism.
 *
 * @see java.io.Serializable
 * @see GiftStorage for more info about implemented methods.
 */
public abstract class FileGiftStorage implements GiftStorage {

    protected final String filePath;

    protected FileGiftStorage(final String filePath) {
        this.filePath = filePath;
    }
}
