package com.task2.model;

import java.io.Serializable;

/**
 * Lollipop is class that extends sweet class.
 */

public class Lollipop extends Sweet implements Serializable {
    public enum FillingOfLollipop {
        Jam,
        Alcohol,
        Other,
        None
    }

    private FillingOfLollipop filling;

    public Lollipop() {
    }

    public Lollipop(final String name, final String producer,
                    final float weight, final float proteins,
                    final float fats, final float carbohydrates,
                    final float calories, final FillingOfLollipop filling) {
        super(name, producer, weight, proteins, fats, carbohydrates, calories);
        this.filling = filling;
    }

    public FillingOfLollipop getFilling() {
        return filling;
    }

    public void setFilling(FillingOfLollipop filling) {
        this.filling = filling;
    }

    public String toString() {
        return "Lollipop{" + super.toString() +
               ", filling= " + filling +
               '}';

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Lollipop lollipop = (Lollipop) o;

        return filling == lollipop.filling;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (filling != null ? filling.hashCode() : 0);
        return result;
    }
}
