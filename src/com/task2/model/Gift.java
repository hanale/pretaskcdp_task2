package com.task2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.task2.model.comparator.GiftComparatorByWeight;
import com.task2.exceptions.AbsentSweetInGiftException;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class Gift is a list of sweets
 */
public class Gift implements Serializable {
    private List<Sweet> sweetCollection = new ArrayList<>();

    public Gift() {
    }

    /**
     * Method adds a new sweet to gift
     *
     * @param newSweet - collection of sweets
     * @throws Exception if gift collection is null
     */
    public void add(final Sweet newSweet) throws Exception {
        if (newSweet == null) {
            return;
        }
        sweetCollection.add(newSweet);
    }

    /**
     * The method remove sweet from gift
     *
     * @param sweet - sweet that should be removed
     * @throws AbsentSweetInGiftException - if no sweet in gift
     */
    public boolean remove(final Sweet sweet) throws AbsentSweetInGiftException {
        if (sweet == null) {
            return false;
        }

        if (sweetCollection.contains(sweet)) {
            sweetCollection.remove(sweet);
            return true;
        } else {
            throw new AbsentSweetInGiftException("Gift doesn't contain such sweet: ", sweet);
        }

    }

    /**
     * The method find weight of all sweets in gift
     *
     * @return weight of gift
     */
    @JsonIgnore
    public float getGiftWeight() {
        float giftWeight = 0;
        for (Sweet sweet : sweetCollection) {
            giftWeight += sweet.getWeight();
        }
        return giftWeight;
    }

    /**
     * Method sort sweets in gift by weight
     */
    public void sortByWeight() {
        Collections.sort(this.sweetCollection, new GiftComparatorByWeight());
    }

    /**
     * Method finds all sweets in gift that have weight and calories less than entered
     *
     * @param weight   - Max weight of sweet
     * @param calories -Max calories value of sweet
     * @return collection of sweets that have weight and calories less than entered
     */
    public List<Sweet> findSweetsLessThanWeightAndCalories(final float weight, final float calories) {
        return sweetCollection.stream()
                              .filter(sweet -> isSweetLessThanWeightAndCalories(sweet, weight, calories))
                              .collect(Collectors.toList());
    }

    public List<Sweet> getSweetCollection() {
        return sweetCollection;
    }

    public void setSweetCollection(final List<Sweet> sweetCollection) {
        this.sweetCollection = sweetCollection;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("The gift contains the following sweets: " + System.lineSeparator());
        for (Sweet sweet : sweetCollection) {
            builder.append(sweet.toString()).append(System.lineSeparator());
        }
        return builder.toString();
    }

    private boolean isSweetLessThanWeightAndCalories(final Sweet sweet, final float weight, final float calories) {
        return sweet != null && sweet.getWeight() < weight && sweet.getCalories() < calories;
    }
}
