package com.task2.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Sweet class describes sweet object
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class Sweet implements Serializable {
    private String name;
    private String producer;
    private float weight;
    private float proteins;
    private float fats;
    private float carbohydrates;
    private float calories;

    protected Sweet() {

    }

    public Sweet(final String name, final String producer,
                 final float weight, final float proteins,
                 final float fats, final float carbohydrates,
                 final float calories) {
        this.name = name;
        this.producer = producer;
        this.weight = weight;
        this.proteins = proteins;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getProteins() {
        return proteins;
    }

    public void setProteins(float proteins) {
        this.proteins = proteins;
    }

    public float getFats() {
        return fats;
    }

    public void setFats(float fats) {
        this.fats = fats;
    }

    public float getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(float carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public float getCalories() {
        return calories;
    }

    public void setCalories(float calories) {
        this.calories = calories;
    }

    public String toString() {
        return
                "name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", weight=" + weight +
                ", proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                ", calories=" + calories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Sweet sweet = (Sweet) o;

        if (Float.compare(sweet.weight, weight) != 0) {
            return false;
        }
        if (Float.compare(sweet.proteins, proteins) != 0) {
            return false;
        }
        if (Float.compare(sweet.fats, fats) != 0) {
            return false;
        }
        if (Float.compare(sweet.carbohydrates, carbohydrates) != 0) {
            return false;
        }
        if (Float.compare(sweet.calories, calories) != 0) {
            return false;
        }
        if (name != null ? !name.equals(sweet.name) : sweet.name != null) {
            return false;
        }
        return producer != null ? producer.equals(sweet.producer) : sweet.producer == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (producer != null ? producer.hashCode() : 0);
        result = 31 * result + (weight != +0.0f ? Float.floatToIntBits(weight) : 0);
        result = 31 * result + (proteins != +0.0f ? Float.floatToIntBits(proteins) : 0);
        result = 31 * result + (fats != +0.0f ? Float.floatToIntBits(fats) : 0);
        result = 31 * result + (carbohydrates != +0.0f ? Float.floatToIntBits(carbohydrates) : 0);
        result = 31 * result + (calories != +0.0f ? Float.floatToIntBits(calories) : 0);
        return result;
    }
}
