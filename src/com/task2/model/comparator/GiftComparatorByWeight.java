package com.task2.model.comparator;

import com.task2.model.Sweet;

import java.util.Comparator;

/**
 *
 */
public class GiftComparatorByWeight implements Comparator<Sweet> {

    public int compare(final Sweet o1, final Sweet o2) {
        float temp = o1.getWeight() - o2.getWeight();
        if (temp > 0) {
            return 1;
        } else if (temp < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
