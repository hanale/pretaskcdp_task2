package com.task2.model;

import java.io.Serializable;

/**
 * Chocolate is class that extends sweet class.
 */

public class Chocolate extends Sweet implements Serializable {
    public enum ColorOfChocolate {
        Black,
        White,
        Milk,
        Unknown
    }

    private ColorOfChocolate color;

    public Chocolate() {
    }

    public Chocolate(final String name, final String producer,
                     final float weight, final float proteins,
                     final float fats, final float carbohydrates,
                     final float calories, final ColorOfChocolate color) {
        super(name, producer, weight, proteins, fats, carbohydrates, calories);
        this.color = color;
    }

    public ColorOfChocolate getColor() {
        return color;
    }

    public void setColor(ColorOfChocolate color) {
        this.color = color;
    }

    public String toString() {
        return "Chocolate{" + super.toString() +
               ", color= " + color +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Chocolate chocolate = (Chocolate) o;

        return color == chocolate.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

}
