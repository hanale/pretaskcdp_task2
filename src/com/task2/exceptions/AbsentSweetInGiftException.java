package com.task2.exceptions;

import com.task2.model.Sweet;

/**
 * Exception appear when user tries to remove non-existing sweet
 */
public class AbsentSweetInGiftException extends Exception {
    private Sweet sweet;

    public Sweet getSweet() {
        return sweet;
    }

    public AbsentSweetInGiftException(final String message, final Sweet sweet) {
        super(message + sweet.toString());
        this.sweet = sweet;
    }
}
