package com.task2.utils;

import com.task2.exceptions.AbsentSweetInGiftException;
import com.task2.model.Chocolate;
import com.task2.model.Gift;
import com.task2.model.Lollipop;
import com.task2.model.Sweet;
import com.task2.storage.GiftStorage;
import com.task2.storage.SimpleTxtFileGiftStorage;
import com.task2.storage.StorageFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.task2.utils.ConsoleReader.*;

public class GiftOperation {
    private static final int DEFAULT_USER_CHOICE = -1;

    private ConsoleReader consoleReader;

    public GiftOperation(ConsoleReader consoleReader) {
        this.consoleReader = consoleReader;
    }

    public Gift giftReader() throws IOException {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int userChoiceReader = DEFAULT_USER_CHOICE;
        while (!isInputInvalid(userChoiceReader, 1, 4)) {
            try {
                System.out.println("Select resource for get gift: " + System.lineSeparator() +
                        "1 - .txt file" + System.lineSeparator() +
                        "2 - Json" + System.lineSeparator() +
                        "3 - DB" + System.lineSeparator() +
                        "4 - program initialization ");
                userChoiceReader = Integer.parseInt(readerOfKeyBoard.readLine());
            } catch (Exception e) {
                System.out.println("You enter invalid source. Please enter valid one... ");
            }
        }
        return getGift(userChoiceReader);
    }

    private boolean isInputInvalid(final int userInput, final int min, final int max) {
        return (userInput >= min) && (userInput <= max);
    }

    public void operationUnderGift(final Gift gift) throws IOException {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int userChoiceReader = DEFAULT_USER_CHOICE;
        while (!isInputInvalid(userChoiceReader, 1, 3)) {
            try {
                System.out.println("What do you what to do with gift: " + System.lineSeparator() +
                        "1 - sort by weight" + System.lineSeparator() +
                        "2 - remove sweet from gift" + System.lineSeparator() +
                        "3 - find sweets less than defined weight and calories");
                userChoiceReader = Integer.parseInt(readerOfKeyBoard.readLine());
            } catch (Exception e) {
                System.out.println("You enter invalid operation. Please enter valid one... ");
            }
        }
        userChoiceOperationUnderGift(gift, userChoiceReader);
    }

    public void giftWriter(final Gift gift) throws IOException {
        if (gift == null) {
            return;
        }
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int userChoiceWriter = DEFAULT_USER_CHOICE;
        while (!isInputInvalid(userChoiceWriter, 1, 2)) {
            try {
                System.out.println("Select resource for write gift: " + System.lineSeparator() +
                        "1 - .txt file" + System.lineSeparator() +
                        "2 - console");
                userChoiceWriter = Integer.parseInt(readerOfKeyBoard.readLine());
            } catch (Exception e) {
                System.out.println("You enter invalid source. Please enter valid one... ");
            }
        }
        writeGift(gift, userChoiceWriter);
    }

    private void userChoiceOperationUnderGift(final Gift gift, final int userOperation) {
        switch (userOperation) {
            case 1:
                gift.sortByWeight();
                System.out.println("The gift after sorting" + gift.toString());
                break;
            case 2:
                try {
                    removeSweetFromGift(gift);
                } catch (IOException e) {
                    System.out.println("The sweet is absent in gift. " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    findSweetInGift(gift);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void removeSweetFromGift(final Gift gift) throws IOException {
        try {
            if (gift.remove(sweetReader())) {
                System.out.println("The sweet is removed from gift. ");
            }
        } catch (AbsentSweetInGiftException e) {
            System.out.println("Sweet wasn't removed from gift. Sorry.");
        }
    }

    private void findSweetInGift(final Gift gift) throws Exception {
        float weight = consoleReader.readFloatNumber("weight");
        float calories = consoleReader.readFloatNumber("calories");
        System.out.println(gift.findSweetsLessThanWeightAndCalories(weight, calories));
    }

    private Sweet sweetReader() {
        Sweet sweetForRemoving = null;
        try {
            switch (consoleReader.getTypeOfSweet()) {
                case 1:
                    sweetForRemoving = getChocolate();
                    break;
                case 2:
                    sweetForRemoving = getLollipop();
            }
        } catch (IOException e) {
            sweetForRemoving = new Chocolate("qw", "sdf", 64, 0, 7, 8, 3, Chocolate.ColorOfChocolate.Black);
        }
        return sweetForRemoving;
    }

    private Lollipop getLollipop() throws IOException {
        String name = consoleReader.readStringValue("name");
        String producer = consoleReader.readStringValue("producer");
        float weight = consoleReader.readFloatNumber("weight");
        float proteins = consoleReader.readFloatNumber("proteins");
        float fats = consoleReader.readFloatNumber("fats");
        float carbohydrates = consoleReader.readFloatNumber("carbohydrates");
        float calories = consoleReader.readFloatNumber("calories");
        Lollipop.FillingOfLollipop typeOfFilling = consoleReader.getTypeOfFilling();
        return new Lollipop(name, producer, weight, proteins, fats, carbohydrates, calories, typeOfFilling);
    }

    private Chocolate getChocolate() throws IOException {
        String name = consoleReader.readStringValue("name");
        String producer = consoleReader.readStringValue("producer");
        float weight = consoleReader.readFloatNumber("weight");
        float proteins = consoleReader.readFloatNumber("proteins");
        float fats = consoleReader.readFloatNumber("fats");
        float carbohydrates = consoleReader.readFloatNumber("carbohydrates");
        float calories = consoleReader.readFloatNumber("calories");
        Chocolate.ColorOfChocolate typeOfChocolate = consoleReader.getTypeOfChocolate();
        return new Chocolate(name, producer, weight, proteins, fats, carbohydrates, calories, typeOfChocolate);
    }

    private void writeGift(final Gift gift, final int storageType) {
        switch (storageType) {
            case 1:
                try {
                    GiftStorage storage = new SimpleTxtFileGiftStorage(consoleReader.getFilePath());
                    storage.writeToStorage(gift);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
            default:
                System.out.println(gift.toString());
        }
    }

    private Gift getGift(final int userChoice) {
        Gift gift = null;
        switch (userChoice) {
            case 1:
            case 2:
                boolean allOk = false;
                while (!allOk) {
                    try {
                        gift = getGiftFromFile(userChoice);
                        allOk = true;
                    } catch (Exception e) {
                        System.out.println("Error during reading from file: " + e.getMessage());
                        System.out.println("Repeat, please");
                    }
                }
                break;
            case 3:
                try {
                    gift = getGiftFromDb(userChoice);
                } catch (Exception e) {
                    System.out.println("Error during reading from DB: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case 4:
            default:
                gift = createDefaultGift();
        }
        return gift;
    }

    private Gift getGiftFromFile(final int storageType) throws Exception {
        String filePath = consoleReader.getFilePath();
        GiftStorage storage = StorageFactory.getStorage(storageType, filePath);
        return storage.readFromStorage(null);
    }

    private Gift getGiftFromDb(final int storageType) throws Exception {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter gift ID...");
        String giftId = readerOfKeyBoard.readLine();
        GiftStorage storage = StorageFactory.getStorage(storageType, null);
        return storage.readFromStorage(giftId);
    }

    private Gift createDefaultGift() {
        Gift gift = new Gift();
        try {
            gift.add(new Chocolate("qw", "sdf", 64, 0, 7, 8, 3, Chocolate.ColorOfChocolate.Black));
            gift.add(new Chocolate("two", "tt", 0.57f, 4, 6, 5, 8, Chocolate.ColorOfChocolate.Black));
            gift.add(new Chocolate("3", "tt", 34, 4, 6, 5, 4, Chocolate.ColorOfChocolate.Black));
            gift.add(new Chocolate("one", "tt", 2, 4, 6, 5, 4, Chocolate.ColorOfChocolate.Black));
            gift.add(new Chocolate("four", "tt", 34, 4, 6, 5, 4, Chocolate.ColorOfChocolate.Black));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gift;
    }
}
