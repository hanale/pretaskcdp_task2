package com.task2.utils;

import com.task2.model.Chocolate;
import com.task2.model.Lollipop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class allows static methods for receiving user input via console.
 */
public class ConsoleReader {

    private static final int GENERAL_KIND_OF_SWEET_COUNT = 2;

    /**
     * Method receive from user float value via console without any validation.
     * Before reading from console will be printed request to user.
     *
     * @param value Name of the value which will be entered by user
     * @return Float value from user
     * @throws IOException in case of any IO errors.
     */
    public float readFloatNumber(final String value) throws IOException {
        System.out.println("Enter " + value + " value");
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        return Float.parseFloat(readerOfKeyBoard.readLine());
    }

    /**
     * Method receive from user string value via console without any validation.
     * Before reading from console will be printed request to user.
     *
     * @param value Name of the value which will be entered by user
     * @return String value from user
     * @throws IOException in case of any IO errors.
     */
    public String readStringValue(final String value) throws IOException {
        System.out.println("Enter " + value + " value");
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        return readerOfKeyBoard.readLine();
    }

    /**
     * Method receive from user a full path to file as a string value.
     * Method doesn't validate path correctness or if file exist.
     *
     * @return Full file path from user
     * @throws IOException in case of any IO errors.
     */
    public String getFilePath() throws IOException {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter path to file...");
        return readerOfKeyBoard.readLine();
    }

    /**
     * Method receive from user int value via console and convert it into FillingOfLollipop with validation.
     * User should enter one of the proposed options. In case of any other values process will be repeated.
     *
     * @return Lollipop.FillingOfLollipop - filling of lollipop. The available values, see in Lollipop
     */
    public Lollipop.FillingOfLollipop getTypeOfFilling() {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int typeOfFilling = 0;
        boolean isTypeOfFillingValid = false;
        while (!isTypeOfFillingValid) {
            try {
                System.out.println("Enter type of filling: 1 - Jam, 2 - Alcohol, 3 - Other, 4 - None");
                typeOfFilling = Integer.parseInt(readerOfKeyBoard.readLine());
                isTypeOfFillingValid = isTypeOfFillingInvalid(typeOfFilling);
            } catch (Exception e) {
                System.out.println("You enter invalid type of filling. Please enter valid one... ");

            }
        }
        return Lollipop.FillingOfLollipop.values()[typeOfFilling];
    }

    /**
     * Method receive from user int value via console and convert it into ColorOfChocolate with validation.
     * User should enter one of the proposed options. In case of any other values process will be repeated.
     *
     * @return ColorOfChocolate chosen by user
     */
    public Chocolate.ColorOfChocolate getTypeOfChocolate() {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int typeOfChocolate = 0;
        boolean isTypeOfChocolateValid = false;
        while (!isTypeOfChocolateValid) {
            try {
                System.out.println("Enter type of chocolate: 1 - Black, 2 - White, 3 - Milk, 4 - Unknown");
                typeOfChocolate = Integer.parseInt(readerOfKeyBoard.readLine());
                isTypeOfChocolateValid = isTypeOfChocolateInvalid(typeOfChocolate);
            } catch (Exception e) {
                System.out.println("You enter invalid type of chocolate. Please enter valid one... ");

            }
        }
        return Chocolate.ColorOfChocolate.values()[typeOfChocolate];
    }


    /**
     * Method receive from user int value via console and convert it into type of sweet with validation.
     * User should enter one of the proposed options. In case of any other values process will be repeated.
     *
     * @return int typeOfSweet -  type of sweet:
     * 1 - Chocolate,
     * 2 - Lollipop
     */
    public int getTypeOfSweet() {
        BufferedReader readerOfKeyBoard = new BufferedReader(new InputStreamReader(System.in));
        int typeOfSweet = 0;
        boolean isTypeOfSweet = false;
        while (!isTypeOfSweet) {
            try {
                System.out.println("Which type of sweet do you want to delete: 1 - Chocolate, 2 - Lollipop");
                typeOfSweet = Integer.parseInt(readerOfKeyBoard.readLine());
                isTypeOfSweet = isTypeOfSweetValid(typeOfSweet);
            } catch (Exception e) {
                System.out.println("You enter invalid type of sweet. Please enter valid one... ");

            }
        }
        return typeOfSweet;
    }


    private boolean isTypeOfSweetValid(final int userInput) {
        return (userInput >= 1) && (userInput <= GENERAL_KIND_OF_SWEET_COUNT);
    }

    private boolean isTypeOfFillingInvalid(final int userInput) {
        return (userInput >= 1) && (userInput <= Lollipop.FillingOfLollipop.values().length);
    }

    private boolean isTypeOfChocolateInvalid(final int userInput) {
        return (userInput >= 1) && (userInput <= Chocolate.ColorOfChocolate.values().length);
    }
}
