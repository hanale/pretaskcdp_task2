package com.task2;

import com.task2.model.Gift;
import com.task2.utils.ConsoleReader;
import com.task2.utils.GiftOperation;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            GiftOperation giftOperation = new GiftOperation(new ConsoleReader());
            Gift gift = giftOperation.giftReader();
            giftOperation .operationUnderGift(gift);
            giftOperation .giftWriter(gift);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
