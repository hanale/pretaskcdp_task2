CREATE SEQUENCE public.gifts_sweet_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.gifts
(
  sweet_id integer NOT NULL DEFAULT nextval('gifts_sweet_id_seq'::regclass),
  gift_id numeric NOT NULL,
  sweet_name character varying(250) NOT NULL,
  sweet_type character varying(250) NOT NULL,
  sweet_producer character varying(250) NOT NULL,
  sweet_weight numeric NOT NULL,
  sweet_proteins numeric NOT NULL,
  sweet_fats numeric NOT NULL,
  sweet_carbohydrates numeric NOT NULL,
  sweet_calories numeric NOT NULL,
  sweet_filling character varying(250),
  sweet_color character varying(250),
  CONSTRAINT pk_gifts PRIMARY KEY (sweet_id)
);

INSERT INTO public.gifts(
                  sweet_id, gift_id, sweet_name, sweet_type, sweet_producer, sweet_weight,
                  sweet_proteins, sweet_fats, sweet_carbohydrates, sweet_calories,
                  sweet_filling, sweet_color)
VALUES (1,1,'AlpenGold','Chocolate','Moscow',34,45,67,78,89,null,'Black'),
(2,1,'AlpenGold Red','Chocolate','Moscow',45,67,89,90,45,null,'Black'),
(3,1,'AlpenGold White','Chocolate','Moscow',34,45,67,78,89,null,'White'),
(4,1,'AlpenGold Milk','Chocolate','Moscow',34,45,67,78,89,null,'Milk'),
(5,1,'Swet 2','Lollipop','Moscow',34,45,67,78,89,'Alcohol',null),
(6,1,'Swet 1','Lollipop','Moscow',34,45,67,78,89,'Other',null),
(7,2,'Swet 1','Lollipop','Moscow',34,45,67,78,89,'Other',null);
